#!/bin/sh

### create a folder for the DUNE and DuMuX modules
### go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

# dune-common
# master # 7c5c102857f1ab9fccfb0397a732735b8cc67cd8 # 2021-02-04 11:41 +0100 # Simon Praetorius
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout master
git reset --hard 7c5c102857f1ab9fccfb0397a732735b8cc67cd8
cd ..

# dune-geometry
# master # 1a7dddcfd2c4a28d851dac38555914148d161171 # 2021-01-31 09:10 +0100 # Andreas Dedner
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout master
git reset --hard 1a7dddcfd2c4a28d851dac38555914148d161171
cd ..

# dune-grid
# master # a9ca0ade840ba2f48a2556995186ab94f31bbafe # 2021-02-02 07:18 +0100 # Robert K
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout master
git reset --hard a9ca0ade840ba2f48a2556995186ab94f31bbafe
cd ..

# dune-localfunctions
# master # f6dcf39c79f04643584a12fdb27c775aaf717235 # 2021-01-23 15:00 +0100 # Simon Praetorius
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout master
git reset --hard f6dcf39c79f04643584a12fdb27c775aaf717235
cd ..

# dune-istl
# master # b7f4a70eb80fe4c70c5270e5c8cddedf68808303 # 2021-02-01 22:34 +0100 # Christian Engwer
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout master
git reset --hard b7f4a70eb80fe4c70c5270e5c8cddedf68808303
cd ..

# dune-foamgrid
# master # bd68a6ff42136621c5321acabea17ac10d4e3997 # 2020-04-02 12:03 +0200 # Timo Koch
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout master
git reset --hard bd68a6ff42136621c5321acabea17ac10d4e3997
cd ..

# dune-subgrid
# master # 1bc0b375ffdcf39222416ecafea47db6f08e91f6 # 2020-05-06 17:13 +0200 # Oliver Sander
git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git
cd dune-subgrid
git checkout master
git reset --hard 1bc0b375ffdcf39222416ecafea47db6f08e91f6
cd ..

# dumux
# feature/pore-network-model # d2a63ce48a7f035d4b26060dac747d5acaa81551 # 2021-02-08 15:08 +0100 # Kilian Weishaupt
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout feature/pore-network-model
cd ..

# this module
git clone https://git.iws.uni-stuttgart.de/dumux-pub/michalkowski2021a.git

# run dunecontrol
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all

# clean up
rm installMichalkowski2021a.sh
