
/*****************************************************************************
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A fluid system with one phase and 2 components: interstitial fluid and another component.
 */
#ifndef DUMUX_INTERSTITIAL_TWOC_FLUID_SYSTEM_HH
#define DUMUX_INTERSTITIAL_TWOC_FLUID_SYSTEM_HH

#include <dune/common/exceptions.hh>

#include <dumux/common/propertysystem.hh>
#include <dumux/material/fluidsystems/base.hh>
#include <dumux/material/components/interstitialfluid.hh>

namespace Dumux
{
namespace FluidSystems
{
/*!
 * \brief A fluid system with one phase and 2 components: interstitial fluid and another component.
 */
template <class TypeTag>
class InterstitialFluidTwoC
 : public BaseFluidSystem<typename GET_PROP_TYPE(TypeTag, Scalar), InterstitialFluidTwoC<TypeTag> >
{
    using ThisType = InterstitialFluidTwoC<TypeTag>;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Base = BaseFluidSystem<Scalar, ThisType>;
    using InterstitialFluid = Dumux::InterstitialFluid<TypeTag>;

public:
    //! Number of phases in the fluid system
    static constexpr int numPhases = 1;

    //! Number of components in the fluid system
    static constexpr int numComponents = 2;

    //! Index of the liquid phase
    static constexpr int lPhaseIdx = 0;

    enum {
        // component indices
        interstitialFluidIdx = 0,
        transportCompIdx = 1
    };

    /*!
     * \brief Return the human readable name of a fluid phase.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static std::string phaseName(int phaseIdx)
    { return InterstitialFluid::name(); }

    /*!
     * \brief Return whether a phase is liquid.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isLiquid(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be compressible.
     *
     * Compressible means that the partial derivative of the density
     * to the fluid pressure is always larger than zero.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isCompressible(int phaseIdx)
    {
       assert(0 <= phaseIdx && phaseIdx < numPhases);
       // interstitialfluid is assumed to be incompressible and newtonian
       return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal gas.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealGas(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return false;
    }

    /*!
     * \brief Return the human readable name of a component
     */
    static std::string componentName(int compIdx)
    {
        switch(compIdx)
        {
        case interstitialFluidIdx:
            return InterstitialFluid::name();
        case transportCompIdx:
            static const std::string tname = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, InterstitialFluid, ComponentName);
            return tname;
        default:
            DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
        }
    }

    /*!
     * \brief Return the molar mass of a component [kg/mol].
     */
    static Scalar molarMass(int compIdx)
    {
        // we assume that the component doesn't influence the molar mass
        return InterstitialFluid::molarMass();
    }

    static void init()
    {}

    /*!
     * \brief Given all mole fractions in a phase, return the phase
     *        density [kg/m^3].
     */
    using Base::density;
    template <class FluidState>
    static Scalar density(const FluidState &fluidState, int phaseIdx)
    {
        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);
        return InterstitialFluid::liquidDensity(temperature, pressure);
    }

    /*!
     * \brief Return the dynamic viscosity of a phase [Pa*s].
     */
    using Base::viscosity;
    template <class FluidState>
    static Scalar viscosity(const FluidState &fluidState, int phaseIdx)
    {
        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);
        return InterstitialFluid::liquidViscosity(temperature, pressure);
    }

    /*!
     * \brief Return the diffusion
     *        coefficent of the transported component in water
     */
    using Base::diffusionCoefficient;
    template <class FluidState>
    static Scalar diffusionCoefficient(const FluidState &fluidState, int phaseIdx, int compIdx)
    { DUNE_THROW(Dune::NotImplemented, "Diffusion coefficients"); }

    /*!
     * \brief Given a phase's composition, temperature and pressure,
     *        return the binary diffusion coefficient [m^2/s] for components
     *        \f$i\f$ and \f$j\f$ in this phase.
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIIdx The index of the first component to consider
     * \param compJIdx The index of the second component to consider
     */
    using Base::binaryDiffusionCoefficient;
    template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState &fluidState, int phaseIdx, int compIIdx, int compJIdx)
    {
        static const Scalar D = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, InterstitialFluid, ComponentDiffusionCoefficient);
        return D;
    }

};

} // end namespace FluidSystems

} // end namespace Dumux

#endif
