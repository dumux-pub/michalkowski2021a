// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief Interstitial fluid as a single phase fluid
 */
#ifndef DUMUX_INTERSTITIALFLUID_HH
#define DUMUX_INTERSTITIALFLUID_HH

#include <dumux/material/components/component.hh>

namespace Dumux
{
/*!
 * \ingroup Components
 *
 * \brief InterstitialFluid component (controlled by the input file)
 *
 * \tparam Scalar The type used for scalar values
 */
template <class TypeTag>
class InterstitialFluid : public Component<typename GET_PROP_TYPE(TypeTag, Scalar), InterstitialFluid<TypeTag> >
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

public:
    /*!
     * \brief A human readable name for interstitial fluid.
     */
    static std::string name()
    { return "InterstitialFluid"; }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of interstitial (rough estimate).
     */
    static Scalar molarMass()
    {
        static const Scalar molarMass = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, InterstitialFluid, MolarMass);
        return molarMass;
    }

    /*!
     * \brief Returns true if the liquid phase is assumed to be compressible
     */
    static bool liquidIsCompressible()
    { return false; }

    /*!
     * \brief The density of pure water at a given pressure and temperature \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidDensity(Scalar temperature, Scalar pressure)
    {
        static const Scalar density = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, InterstitialFluid, Density);
        return density;
    }

    /*!
     * \brief The molar density of pure water at a given pressure and temperature \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar molarDensity(Scalar temperature, Scalar pressure)
    {
        return liquidDensity(temperature, pressure)/molarMass();
    }

    /*!
     * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of pure water.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidViscosity(Scalar temperature, Scalar pressure)
    {
        static const Scalar viscosity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, InterstitialFluid, Viscosity);
        return viscosity;
    }

};

} // end namespace Dumux

#endif
