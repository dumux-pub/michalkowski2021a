// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief Blood as a single phase fluid
 */
#ifndef DUMUX_MATERIAL_COMPONENTS_BLOOD_HH
#define DUMUX_MATERIAL_COMPONENTS_BLOOD_HH

#include <dumux/common/parameters.hh>
#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 *
 * \brief Blood component (controlled by the input file)
 *
 * \tparam Scalar The type used for scalar values
 */
template <class Scalar>
class Blood
: public Components::Base<Scalar, Blood<Scalar> >
, public Components::Liquid<Scalar, Blood<Scalar> >
{
public:
    /*!
     * \brief A human readable name for blood.
     */
    static std::string name()
    { return "Blood"; }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of blood
     */
    static Scalar molarMass()
    {
        static const Scalar molarMass = getParam<Scalar>("Blood.MolarMass", 0.018);
        return molarMass;
    }

    /*!
     * \brief Returns true if the liquid phase is assumed to be compressible
     */
    static constexpr bool liquidIsCompressible()
    { return false; }

    /*!
     * \brief The density of pure water at a given pressure and temperature \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidDensity(Scalar temperature, Scalar pressure)
    {
        static const Scalar density = getParam<Scalar>("Blood.LiquidDensity", 1045.0);
        return density;
    }

    /*!
     * \brief The molar density of pure water at a given pressure and temperature \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar molarDensity(Scalar temperature, Scalar pressure)
    {
        return liquidDensity(temperature, pressure)/molarMass();
    }

    /*!
     * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of pure water.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidViscosity(Scalar temperature, Scalar pressure)
    {
        static const Scalar kinematicViscosity = getParam<Scalar>("Blood.PlasmaLiquidKinematicViscosity", 1.3e-6);
        return kinematicViscosity * liquidDensity(temperature, pressure);
    }

};

} // end namespace Components
} // end namespace Dumux

#endif
