// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Implementation of a regularized version of the pore network
 *        capillary pressure / relative permeability  <-> saturation relation for droplet related problems.
 */
#ifndef DUMUX_PNM_TEST_DROPLET_LOCAL_RULES_HH
#define DUMUX_PNM_TEST_DROPLET_LOCAL_RULES_HH

#include <dumux/material/fluidmatrixinteractions/porenetwork/pore/2p/regularizedlocalrules.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/pore/2p/baselocalrules.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/spline.hh>

namespace Dumux
{

/*!\ingroup fluidmatrixinteractionslaws
 *
 * \brief Implementation of the regularized  pore network
 *        capillary pressure / relative permeability  <-> saturation relation.
 *        This class bundles the "raw" curves as
 *        static members and doesn't concern itself converting
 *        absolute to effective saturations and vice versa.
 *
 *        In order to avoid very steep gradients the marginal values are "regularized".
 *        This means that in stead of following the curve of the material law in these regions, some linear approximation is used.
 *        Doing this is not worse than following the material law. E.g. for very low wetting phase values the material
 *        laws predict infinite values for \f$\mathrm{p_c}\f$ which is completely unphysical. In case of very high wetting phase
 *        saturations the difference between regularized and "pure" material law is not big.
 *
 *        Regularizing has the additional benefit of being numerically friendly: Newton's method does not like infinite gradients.
 *
 *        The implementation is accomplished as follows:
 *        - check whether we are in the range of regularization
 *         - yes: use the regularization
 *         - no: forward to the standard material law.
 *
 *         For an example figure of the regularization: RegularizedVanGenuchten
 *
 * \see PNMLocalRules
 */
template<class ScalarT>
class LocalRulesDroplet : public RegularizedTwoPLocalRulesBase
{
	using LocalRules = RegularizedTwoPLocalRulesCubeJoekarNiasar<ScalarT>;
public: 

    using Scalar = ScalarT;
    using Params = typename RegularizedTwoPLocalRulesBase::Params<Scalar>;

    static constexpr bool supportsMultipleGeometries()
    { return false; }

 /*!
 * \brief The capillary pressure-saturation curve according to Joekar-Niasar et al., 2010.
 *
 * Empirical  capillary pressure <-> saturation
 * function is given by
 *
 *  \f$\mathrm{
 *  p_C = \frac{2*\sigma}{R(1-e^{-6.83S})}
 *  }\f$
 *
 * \param sw Saturation of the wetting phase \f$\mathrm{[\overline{S}_w]}\f$
 * \param params A container object that is populated with the appropriate coefficients for the respective law.
 *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen,
 *                  and then the params container is constructed accordingly. Afterwards the values are set there, too.
 * \param poreRadius The pore body radius
 * \return Capillary pressure calculated by Joekar-Niasar et al., 2010 constitutive relation.
 */
    static Scalar pc_hydrophobic (const Params& params, const Scalar sw)
    {
        assert(0 <= sw && sw <= 1);
        const Scalar theta = params.contactAngle;
	    const Scalar poreRadius = params.poreRadius;
	    const Scalar sigma = params.surfaceTension;
	    const Scalar cosTheta = std::cos(theta) ;

        return 2*sigma*cosTheta / (poreRadius*(1 - std::exp(-6.83*sw))) ;
    }
 /*!
* \brief The capillary pressure-saturation curve for mixed-wet pores with one hydrophilic side
 *
 * Empirical  capillary pressure <-> saturation
 * derived from analytical considerations and adaptions from Joekar-Niasar et al., 2010 
 *
 * \param sw Saturation of the wetting phase \f$\mathrm{[\overline{S}_w]}\f$
 * \param params A container object that is populated with the appropriate coefficients for the respective law.
 *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen,
 *                  and then the params container is constructed accordingly. Afterwards the values are set there, too.
 * \param poreRadius The pore body radius
 * \return Capillary pressure constitutive relation.
 */
    static Scalar pc_drop(const Params& params, const Scalar sw)
    {
        assert(0 <= sw && sw <= 1);
        const Scalar theta = params.contactAngle;
        const Scalar poreRadius = params.poreRadius;
        const Scalar poreVolume = 8*poreRadius*poreRadius*poreRadius ;//assume rectangular pore
        const Scalar dropVolume = (1-sw)*poreVolume;
        const Scalar dropRadius = std::sqrt(2 * dropVolume / (theta - std::sin(theta))); //check formulation
        const Scalar sigma = params.surfaceTension;
        return 2*sigma / (dropRadius) ;
    }

/*!
* \brief The capillary pressure-saturation curve for mixed-wet pores with one hydrophilic side
 *
 * Empirical  capillary pressure <-> saturation
 * derived from analytical considerations and adaptions from Joekar-Niasar et al., 2010 
 *
 * \param pc capillary pressure
 * \param params A container object that is populated with the appropriate coefficients for the respective law.
 *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen,
 *                  and then the params container is constructed accordingly. Afterwards the values are set there, too.
 * \param poreRadius The pore body radius
 * \return wetting phase saturation constitutive relation.
 */
    static Scalar sw_drop(const Params& params, const Scalar pc)
    {
        const Scalar theta = params.contactAngle;
        const Scalar poreRadius = params.poreRadius;
        const Scalar sigma = params.surfaceTension;
        const Scalar poreVolume = 8*poreRadius*poreRadius*poreRadius;
		    
        return 1- (2*sigma*sigma/(pc*pc)*(theta-std::sin(theta))/(poreVolume));
    }

 /*!
 * \brief The capillary pressure-saturation curve according to Joekar-Niasar et al., 2010.
 *
 * Empirical  capillary pressure <-> saturation
 * function is given by
 *
 *
 * \param pc capillary pressure
 * \param params A container object that is populated with the appropriate coefficients for the respective law.
 *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen,
 *                  and then the params container is constructed accordingly. Afterwards the values are set there, too.
 * \param poreRadius The pore body radius
 * \return Capillary pressure calculated by Joekar-Niasar et al., 2010 constitutive relation.
 */
    static Scalar sw_hydrophobic(const Params& params, const Scalar pc)
    {
            using std::log;
            const Scalar theta = params.contactAngle;
            const Scalar cosTheta = std::cos(theta) ;
            const Scalar poreRadius = params.poreRadius;
            const Scalar sigma = params.surfaceTension;
           
            return  - 1/6.83* log(1 - 1/poreRadius * 2*sigma*cosTheta / pc);
    }  
/*!
* \brief spatial dependent capillary pressure - saturation relation 
* choose hydrophobic formulation for capillary pressure following Joekar-Niasar 2010 or mixed-wet formulation based on geometric derivations based on position in the network
*/
    static Scalar pc(const Params& params, const Scalar sw)
    {
        const Scalar droplet = params.droplet;
        const Scalar lowSw = params.lowSw;
        const Scalar highSw = params.highSw;

        if (droplet == 1) //droplet
        {
            if (sw < lowSw)
            {
                return pc_drop(params, lowSw) + mLow_(params) * (sw - lowSw);
            }
        auto linearCurveForHighSw = [&]()
        {
            const Scalar slopeHighSw = -pc_drop(params, highSw) / (1.0-highSw);
            return slopeHighSw*(sw - 1.0);
        };

        if (sw <= highSw)
            return pc_drop(params, sw); // standard
        else if (sw <= 1.0) // regularized part below sw = 1.0
        {
	    using std::pow;
            if (params.highSwRegularizationMethod == HighSwRegularizationMethod::powerLaw)
                return pc_drop(params, highSw) * pow(((1.0-sw)/(1.0-highSw)), 1.0/3.0);

            else if (params.highSwRegularizationMethod == HighSwRegularizationMethod::linear)
                return linearCurveForHighSw();

            else if (params.highSwRegularizationMethod == HighSwRegularizationMethod::spline)
            {
                // use spline between threshold swe and 1.0
                const Scalar yTh = LocalRules::pc(params, highSw);
                // using zero derivatives at the beginning and end of the spline seems to work best
                // for some reason ...
                Spline<Scalar> sp(highSw, 1.0, // x0, x1
                                    yTh, 0, // y0, y1
                                    0.0, 0.0); // m0, m1

                return sp.eval(sw);
            }
            else
                DUNE_THROW(Dune::NotImplemented, "Regularization not method not implemented");
        }
        else // regularized part above sw = 1.0
             return linearCurveForHighSw();
        }
        else// if (droplet == 0) //rectangular pore
        {   
            return LocalRules::pc(params, sw);
        }
    }
    
/*!
* \brief spatial dependent capillary pressure - saturation relation 
* choose hydrophobic formulation for wetting phase saturation following Joekar-Niasar 2010 or mixed-wet formulation based on geometric derivations based on position in the network
*/
    static Scalar sw(const Params& params, const Scalar pc)
    {
        const Scalar droplet = params.droplet;
        if (droplet == 1) //droplet
        {
            // retrieve the low and the high threshold saturations for the
            // unregularized capillary pressure curve from the parameters
            const Scalar lowSw = params.lowSw;
	    const Scalar highSw = params.highSw;
            const Scalar pcLowSw = pc_drop(params, lowSw);
            const Scalar pcHighSw = pc_drop(params, highSw);

            // low saturation / high pc:
            if(pc > pcLowSw)
            {
                return lowSw + 1/mLow_(params)*(pc - pcLowSw);
            }

            // high saturation / low pc:
            if(pc < pcHighSw)
            {
                if (params.highSwRegularizationMethod == HighSwRegularizationMethod::powerLaw)
		    {
		        auto result = pc/pcHighSw * pc/pcHighSw * pc/pcHighSw * (1.0-highSw);
		        return 1.0 - result;
		    }
		    else if (params.highSwRegularizationMethod == HighSwRegularizationMethod::linear)
		    {
		        const Scalar slopeHighSw = -pcHighSw / (1.0-highSw);
		        return pc/slopeHighSw + 1.0;
		    }
		    else if (params.highSwRegularizationMethod == HighSwRegularizationMethod::spline)
		    {
		        const Scalar yTh = pc_drop(params, highSw);
		        // invert spline between threshold swe and 1.0
		        Spline<Scalar> sp(highSw, 1.0, // x0, x1
		                          yTh, 0, // y0, y1
		                          0.0, 0.0); // m0, m1

		        return sp.intersectInterval(highSw, 1.0,
		                                    0, 0, 0, pc);
		    }
		    else
		        DUNE_THROW(Dune::NotImplemented, "Regularization not method not implemented");
		    }
            return sw_drop(params,pc);
        }
        else// if (droplet == 0) //rectangular pore
            return LocalRules::sw(params, pc);
    }

/*!
     * \brief The partial derivative of the capillary
     *        pressure w.r.t. the effective saturation.
     *
     *
     * \param swe Effective saturation of the wetting phase \f$\mathrm{[\overline{S}_w]}\f$
     * \param params A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     * \param poreRadius The pore body radius
     */
    static Scalar dpc_dsw_drop(const Params &params, const Scalar sw)
    {
        assert(0 <= sw && sw <= 1);
        const Scalar theta = params.contactAngle;
        const Scalar sigma = params.surfaceTension;
        const Scalar poreVolume = 8*params.poreRadius*params.poreRadius*params.poreRadius;

        return 2*sigma*std::sqrt(theta-std::sin(theta))/(std::sqrt(2*poreVolume))*1/(2*std::sqrt((1-sw)*(1-sw)*(1-sw)));
    }

    /*!
     * \brief DOCU
     *
     *
     * \param sw Saturation of the wetting phase \f$\mathrm{[\overline{S}_w]}\f$
     * \param params A container object that is populated with the appropriate coefficients for the respective law.
     */
    static Scalar dsw_dpc(const Params& params, const Scalar sw)
    {
        return 0; // TODO
    }


private:

    /*!
     * \brief   The slope of the straight line used to regularize
     *          saturations below the minimum saturation.
     *
     * \param params A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen,
     *                  and then the params container is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar mLow_(const Params& params)
    {
        return dpc_dsw_drop(params, params.lowSw);
    }
};
} // end namespace Dumux
#endif
