// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief   Parameters that are necessary for the \em regularization of
 *          the PNMLocalRules.
 */
#ifndef DUMUX_DROP_PNM_LOCAL_RULES_PARAMS_HH
#define DUMUX_DROP_PNM_LOCAL_RULES_PARAMS_HH

#include <dumux/common/parameters.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/pore/2p/baselocalrules.hh>

namespace Dumux
{
/*!
 * \brief   Parameters that are necessary for the \em regularization of
 *          the RegularizedPNMLocalRules.
 *
 *        \ingroup fluidmatrixinteractionsparams
 */
template <class Scalar>
class DropPNMLocalRulesParams : public Dumux::TwoPLocalRulesBase
{
public:
    using ParentType = Dumux::TwoPLocalRulesBase::Params<Scalar>;


    Scalar droplet() const noexcept
    { return droplet_;}

    Scalar lowSw() const noexcept
    { return lowSw_;}
    
    Scalar highSw() const noexcept
    { return highSw_;}

    Scalar slopeHighSw() const noexcept
    { return slopeHighSw_;}

    struct DropPNMLocalRulesParamsStruct
    {
        Scalar poreRadius, contactAngle, surfaceTension;
        Pore::Shape shape;
        Scalar lowSw, highSw, droplet;
    };


    static DropPNMLocalRulesParams<Scalar> makeParams(const Scalar poreRadius, const Scalar contactAngle,
                                     const Scalar surfaceTension, const Pore::Shape shape, const Scalar lowSw, const Scalar highSw, const Scalar droplet)
    {
        //static const Scalar lowSw = getParam<Scalar>("Regularization.LowSw", 1e-2);
        //static const Scalar highSw = getParam<Scalar>("Regularization.HighSw", 0.95);
        return DropPNMLocalRulesParamsStruct{poreRadius, contactAngle, surfaceTension, shape, lowSw, highSw, droplet};
    }
private:

Scalar slopeHighSw_;
Scalar lowSw_;
Scalar highSw_;
Scalar droplet_;

};

} // namespace Dumux

#endif
