// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup PNMTwoP
 * \brief Definition of the spatial parameters for the pnm two phase
 *        problem with different wetting properties.
 */
#ifndef DUMUX_PNM_DROPLET_TWOP_SPATIAL_PARAMS_HH
#define DUMUX_PNM_DROPLET_TWOP_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/porenetwork/porenetwork2p.hh>
#include <dumux/material/spatialparams/porenetwork/porenetworkbase.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/throat/thresholdcapillarypressures.hh>
#include "localRulesDroplet.hh"
#include "baselocalrulesDroplet.hh"

namespace Dumux {


template<class GridGeometry, class Scalar, class MaterialLawT>
class PNMDropletTwoPSpatialParams
: public PNMTwoPBaseSpatialParams<GridGeometry, Scalar, MaterialLawT, PNMDropletTwoPSpatialParams<GridGeometry, Scalar, MaterialLawT>>
{
    using ParentType = PNMTwoPBaseSpatialParams<GridGeometry, Scalar, MaterialLawT, PNMDropletTwoPSpatialParams<GridGeometry, Scalar, MaterialLawT>>;
    using GridView = typename GridGeometry::GridView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;

    static const int dim = GridView::dimension;
    static const int dimWorld = GridView::dimensionworld;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:

    using MaterialLaw = MaterialLawT;
    using MaterialLawParams = typename MaterialLaw::Params;

    PNMDropletTwoPSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        if (!gridGeometry->useSameGeometryForAllPores() && MaterialLawT::supportsMultipleGeometries())
            DUNE_THROW(Dune::InvalidStateException, "Your MaterialLaw does not support multiple pore body shapes.");

    }


    template<class GlobalPosition>
   Scalar porosityAtPos(const GlobalPosition& globalPos) const
   { return 1.0; }

    /*!
    * \brief Return the specified contact angle
    *
    * \param element The current element
    */
    template<class ElementVolumeVariables>
    Scalar contactAngle(const Element& element,
                        const ElementVolumeVariables& elemVolVars) const
    {
        static const Scalar wettingangleOne_ = getParam<Scalar>("SpatialParams.WettingAngleOne");
        static const Scalar wettingangleTwo_ = getParam<Scalar>("SpatialParams.WettingAngleTwo");
        const auto& gridGeometry = this->gridGeometry();
        const auto pos = element.geometry().center();
        const auto bBoxMax = gridGeometry.bBoxMax();
        if (pos[1] < 0.55*bBoxMax[1])//&& pos[1] > 0.04*bBoxMax[1])//(pos[1] < 0.705*bBoxMax[1]&& pos[1] > 0.1*bBoxMax[1])
            return wettingangleOne_;
        else 
            return wettingangleTwo_;
    }
    template<class ElementSolutionVector>
    Scalar contactAngle(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolutionVector& elemSol) const
    {   
        static const Scalar wettingangleOne_ = getParam<Scalar>("SpatialParams.WettingAngleOne");
        static const Scalar wettingangleTwo_ = getParam<Scalar>("SpatialParams.WettingAngleTwo");
        const auto& gridGeometry = this->gridGeometry();
        const auto pos = element.geometry().center();
        const auto bBoxMax = gridGeometry.bBoxMax();
        if (pos[1] < 0.55*bBoxMax[1])//&& pos[1] > 0.03*bBoxMax[1])//(pos[1] < 0.705*bBoxMax[1]&& pos[1] > 0.1*bBoxMax[1])
            return wettingangleOne_;
        else 
            return wettingangleTwo_;
    }

    Scalar interfaceConfig(const SubControlVolume& scv) const
    {
        if (this->gridGeometry().poreLabel(scv.dofIndex()) ==Labels::source)
            return 1.0; //droplet
        else 
            return 0.0; //rectangular
    }

    /*!
    * \brief Return the parameter object for the PNM material law
    * 
    * \param element The finite element
    * \param scv The sub-control volume
    * \param elemSol The element solution vector
    */
    template<class ElementSolutionVector>
    MaterialLawParams materialLawParams(const Element& element, 
                                        const SubControlVolume& scv,
                                        const ElementSolutionVector& elemSol) const
    {
        static const Scalar surfaceTension = getParam<Scalar>("SpatialParams.SurfaceTension", 0.0725);
        const Scalar contactAngle = this->asImp_().contactAngle(element,scv,elemSol);
        const Scalar poreRadius = this->asImp_().poreRadius(element,scv,elemSol);
        const auto poreShape = this->gridGeometry().poreGeometry(scv.dofIndex());
        const Scalar droplet = interfaceConfig(scv);
        return MaterialLaw::makeParams(poreRadius, contactAngle, surfaceTension, poreShape, droplet);
    }

private:
    Scalar wettingangleOne_;
    Scalar wettingangleTwo_;
};
} // end namespace Dumux
#endif
