// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase pore network model.
 */
#ifndef DUMUX_PNM2P_PROBLEM_HH
#define DUMUX_PNM2P_PROBLEM_HH

#include <ctime>
#include <iostream>

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porenetworkflow/2p/model.hh>

// spatial params
#include <dumux/material/spatialparams/porenetwork/porenetwork2p.hh>
#include "files/spatialparams.hh"

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/porenetworkflow/common/utilities.hh>
#include "files/h2oair.hh"

#include <dumux/io/gnuplotinterface.hh>

#ifndef ISOTHERMAL
#define ISOTHERMAL 1
#endif

namespace Dumux
{
template <class TypeTag>
class DrainageProblem;

namespace Properties
{
// Create new type tags
namespace TTag {
#if ISOTHERMAL
struct DrainageProblem { using InheritsFrom = std::tuple<PNMTwoP>; };
#else
struct DrainageProblem { using InheritsFrom = std::tuple<PNMTwoPNI>; };
#endif
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::DrainageProblem> { using type = Dumux::DrainageProblem<TypeTag>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DrainageProblem>
  {
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = Dumux::FluidSystems::H2OAir<Scalar, Dumux::Components::SimpleH2O<Scalar>>;
  };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DrainageProblem> { using type = Dune::FoamGrid<1, 3>; };

// Set formulation (pw and sn or pn and sw)
template<class TypeTag>
struct Formulation<TypeTag, TTag::DrainageProblem>
{ static constexpr auto value = TwoPFormulation::p1s0; }; //p1s0 is pnsw


// Set the spatial params  
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DrainageProblem>
{
private: 
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using LocalRules = Dumux::RegularizedTwoPLocalRulesCubeJoekarNiasar<Scalar>;

public:
    using type = PNMWettingSpatialParams<GridGeometry, Scalar, LocalRules>;

};
}
template <class TypeTag>
class DrainageProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using Labels = GetPropType<TypeTag, Properties::Labels>;
    enum {
        // grid and world dimension
        dim = GridView::dimension,
        dimworld = GridView::dimensionworld,

        // primary variable indices
        pnIdx = Indices::pressureIdx,
        swIdx = Indices::saturationIdx,


        // phase indices
        wPhaseIdx = FluidSystem::phase0Idx, //gas
        nPhaseIdx = FluidSystem::phase1Idx, //liquid

#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif
    

    };

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;


public:
    template<class SpatialParams>
    DrainageProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<SpatialParams> spatialParams)
    : ParentType( gridGeometry, spatialParams), eps_(1e-7)
    {
        verbose_ = getParam<bool>("Problem.Verbose", false);
        VtpOutputFrequency_ = getParam<int>("Problem.VtpOutputFrequency");
        pnOutlet_ = getParam<Scalar>("Problem.pnOutlet");
        swInlet_ = getParam<Scalar>("Problem.swInlet");
        pnInlet_ = getParam<Scalar>("Problem.pnInlet");
        saturationThresholdStop_ = getParam<Scalar>("Problem.saturationThresholdStop");
        saturationThresholdStart_ = getParam<Scalar>("Problem.saturationThresholdStart");
        compensationPressure_ = getParam<Scalar>("Problem.compensationPressure");
        throatDiff_ = getParam<Scalar>("Problem.throatDifferenceToPoreSize");

        plotName_ = getParam<std::string>("Plot.Name");
    }

    bool shouldWriteOutput(const int timeStepIndex, const GridVariables& gridVariables) const //define output
    {
        if(VtpOutputFrequency_ < 0)
            return true;
        if(VtpOutputFrequency_ == 0)
            return ( timeStepIndex == 0 || gridVariables.gridFluxVarsCache().invasionState().hasChanged());
        else
            return ( timeStepIndex % VtpOutputFrequency_ == 0 || gridVariables.gridFluxVarsCache().invasionState().hasChanged());
    }

    void postTimeStep(const int timeStepIndex, const GridVariables& gridVariables, SolutionVector& sol, Scalar time)
    {
        const auto& gridView = this->gridGeometry().gridView();
        
        std::vector<double> boundarySaturation;
        boundarySaturation.resize(this->gridGeometry().numScv(),-1);

        Scalar totalSaturation = 0.0;
        Scalar volume = totalVolume(gridVariables, sol);
        
        auto innerBCpores = boundaryPores(gridVariables, sol);
	    
        for (const auto& element : elements(gridView))
        {
	        auto fvGeometry = localView(this->gridGeometry());
	        fvGeometry.bindElement(element);

	        auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);

            for (auto&& scv : scvs(fvGeometry))
            {
                if (std::find(innerBCpores.begin(), innerBCpores.end(), scv.dofIndex()) != innerBCpores.end()){
                    boundarySaturation[scv.dofIndex()] = elemVolVars[scv].saturation(nPhaseIdx);
                }
                if (this->gridGeometry().poreLabel(scv.dofIndex()) !=Labels::inlet && this->gridGeometry().poreLabel(scv.dofIndex()) !=Labels::outlet){
                    totalSaturation += elemVolVars[scv].saturation(nPhaseIdx)*(this->gridGeometry().poreVolume(scv.dofIndex()))/volume;
                }
            }
        }
	x_.push_back(time); // in seconds
        y0_.push_back(totalSaturation);

        int idx = 0;
        int idx2 = 0;
        std::vector<double> boundarySaturationClean;
        for (std::vector<double>::const_iterator i = boundarySaturation.begin(); i != boundarySaturation.end(); ++i){
            if (*i > -1.0){
                boundarySaturationClean.push_back(boundarySaturation.at(idx));
                idx2 +=1;
            }
            idx+=1;
        }
        idx = 0;
        int numberBCPores = count_if (boundarySaturation.begin(), boundarySaturation.end(), [](double i) {return i > -1.0;});
        y_.resize(numberBCPores, std::vector<double>(1));
        for(int i = 0; i < numberBCPores; ++i) {
            (y_)[idx].push_back(boundarySaturationClean.at(idx));
            idx += 1;
        }
    
        gnuplot_.resetPlot();
        gnuplot_.setXRange(0, std::max(time, 1e-10));
        gnuplot_.setYRange(-1e-6, 1);
        gnuplot_.setXlabel("time");
        gnuplot_.setYlabel("saturation");

        for(int i = 0; i < numberBCPores; ++i) {
            gnuplot_.addDataSetToPlot(x_, (y_)[i], "pore"+std::to_string(i)+ "-" + plotName_ + ".dat");
        }

        gnuplot_.plot("saturation_hydrophilicBC");
        gnuplot_.addDataSetToPlot(x_, y0_, "totalSaturation-" + plotName_ + ".dat");
        gnuplot_.plot("totalSaturation_hydrophilicBC");
    }



#if ISOTHERMAL
    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 80; } // 10°C

#endif
     /*!
     * \name Boundary conditions
     */
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolume &scv) const
    {
        BoundaryTypes bcTypes;
        if (isOutletPore_(scv))
           bcTypes.setAllDirichlet();
        else if (isInletPore_(scv))
           bcTypes.setAllDirichlet();
        else // neuman for the remaining boundaries
           bcTypes.setAllNeumann();
#if !ISOTHERMAL
        bcTypes.setDirichlet(temperatureIdx);
#endif
        return bcTypes;
    }

    /*!
    * \brief Evaluate the boundary conditions for a dirichlet
    *        control volume.
    *
    * \param values The dirichlet values for the primary variables
    * \param vertex The vertex (pore body) for which the condition is evaluated
    *
    * For this method, the \a values parameter stores primary variables.
    */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);

        if(isOutletPore_(scv)){
            values[pnIdx] = pnOutlet_; //pressure wetting phase?
            values[swIdx] = 1.0; //saturation non-wetting phase?
        }
        if(isInletPore_(scv)){
            values[pnIdx] = pnInlet_; //pressure wetting phase?
            values[swIdx] = swInlet_; //results in a capillary pressure of 5003.765Pa
        }
#if !ISOTHERMAL
        if(isInletPore_(scv)){
            values[temperatureIdx] = 273.15 + 15;
        }else{
            values[temperatureIdx] = 273.15 + 10;
        }
#endif
        return values;
    }


    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub control volume
     *
     * For this method, the return parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    PrimaryVariables source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);
        if(isSourcePore_(scv) && marker[scv.dofIndex()] == 0){
            if (elemVolVars[scv].saturation(nPhaseIdx) > saturationThresholdStart_){
                values[nPhaseIdx] -= boundaryConductivity(scv) *(elemVolVars[scv].pressure(nPhaseIdx)-compensationPressure_)/(this->gridGeometry().poreVolume(scv.dofIndex()));
                changeMarker(scv); 
            }else{ 
                values[swIdx] +=0.0;
            }
        }
        else if (isSourcePore_(scv) && marker[scv.dofIndex()] == 1){
            if (elemVolVars[scv].saturation(nPhaseIdx) > saturationThresholdStop_){
                values[nPhaseIdx] -= boundaryConductivity(scv) *(elemVolVars[scv].pressure(nPhaseIdx)-compensationPressure_)/(this->gridGeometry().poreVolume(scv.dofIndex()));
            }else{ 
                values[swIdx] +=0.0;
                changeMarker(scv); 
            }
        }
        return values;
    }
    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Vertex& vertex) const
    {
        PrimaryVariables values(0.0);

        values[pnIdx] = pnOutlet_;

        //get global index of pore
        int dofIdxGlobal = this->gridGeometry().vertexMapper().index(vertex);
        if(isInletPore_(dofIdxGlobal))
            values[swIdx] = swInlet_;
        else
            values[swIdx] = 1.0;

#if !ISOTHERMAL
        values[temperatureIdx] = 273.15 + 10;
#endif
        return values;
    }
    Scalar boundaryConductivity(const SubControlVolume &scv) const
    {
        GetPropType<TypeTag, Properties::FluidState> fluidState;
        fluidState.setTemperature(temperature());
        fluidState.setPressure(0, pnOutlet_);

        const Scalar epsilon = 1e-5;
        const Scalar length = epsilon;
        Scalar viscosity = FluidSystem::viscosity(fluidState, nPhaseIdx);
	    const Scalar radius = this->gridGeometry().poreRadius(scv.dofIndex())-throatDiff_;
	    const Scalar rEff = radius;
        const Scalar result = M_PI/(8*length*viscosity) * rEff*rEff*rEff*rEff;
	    return result;
    }
    
    void initMarker()
    {
        marker.resize(this->gridGeometry().numDofs(),0);
    }

    void changeMarker(const SubControlVolume &scv) const
    {
        if (marker[scv.dofIndex()] == 0){
            marker[scv.dofIndex()] = 1;
            std::cout << "marker set for " << scv.dofIndex() << std::endl;
        }else{
            marker[scv.dofIndex()] = 0;
            std::cout << "marker removed for " << scv.dofIndex()<< std::endl;
        }
    }

    auto boundaryPores (const GridVariables& gridVariables, SolutionVector& sol) const
    {
        std::vector<double> boundaryPores;

        const auto& gridView = this->gridGeometry().gridView();

        for (const auto& element : elements(gridView))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);
            
            
            for (auto&& scv : scvs(fvGeometry))
            {
                if (this->gridGeometry().poreLabel(scv.dofIndex()) ==Labels::source)
                    boundaryPores.push_back(scv.dofIndex());
            }
        }
        sort( boundaryPores.begin(), boundaryPores.end() );
        boundaryPores.erase( unique( boundaryPores.begin(), boundaryPores.end() ), boundaryPores.end() );

        return boundaryPores; 
    }

    Scalar totalVolume (const GridVariables& gridVariables, SolutionVector& sol) const
    {
        Scalar totalVolume=0.0;
        
        const auto& gridView = this->gridGeometry().gridView();

        for (const auto& element : elements(gridView))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);
            
            
            for (auto&& scv : scvs(fvGeometry))
            {
                if (this->gridGeometry().poreLabel(scv.dofIndex()) !=Labels::inlet && this->gridGeometry().poreLabel(scv.dofIndex()) !=Labels::outlet){
                    totalVolume += this->gridGeometry().poreVolume(scv.dofIndex());
                }
            }
        }
        return totalVolume;
    }
        
    /*!
     * \brief Evaluate the initial invasion state of a pore throat
     *
     * Returns true for a invaded throat and false elsewise.
     */
    bool initialInvasionState(const Element &element) const
    { return false; }

    const bool verbose() const
    { return verbose_; }

    bool simulationFinished() const
    { return false ; }


private:

    bool isInletPore_(const SubControlVolume& scv) const
    {
        return isInletPore_(scv.dofIndex());
    }

    bool isInletPore_(const std::size_t dofIdxGlobal) const
    {
        return this->gridGeometry().poreLabel(dofIdxGlobal) ==Labels::inlet;
    }
    
    bool isOutletPore_(const SubControlVolume& scv) const
    {
        return this->gridGeometry().poreLabel(scv.dofIndex()) ==Labels::outlet;
    }
    
     bool isSourcePore_(const SubControlVolume& scv) const
    {
        return this->gridGeometry().poreLabel(scv.dofIndex()) ==Labels::source;
    }
   
    
    Scalar eps_;
    bool verbose_;
    int VtpOutputFrequency_;
    Scalar pnOutlet_;
    Scalar swInlet_;
    Scalar pnInlet_;
    Scalar saturationThresholdStart_;
    Scalar saturationThresholdStop_;
    Scalar compensationPressure_;
    Scalar throatDiff_;
    mutable std::vector<int> marker;

    Dumux::GnuplotInterface<double> gnuplot_;

    std::string plotName_;

    std::vector<double> x_;
    std::vector<std::vector<double>> y_;
    std::vector<double> y0_;
};
} //end namespace

#endif
