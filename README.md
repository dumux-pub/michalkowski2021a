Summary
=======
This is the DuMuX module containing the code for producing the results of

* Michalkowski & Helmig & Schleper (2021, submitted): *Modeling of the two phase flow in a hydrophobic porous medium interacting with a hydrophilic structure*

Installation
============

__Dependencies__
This module [requires](https://dune-project.org/doc/installation/)
* a standard c++17 compliant compiler (e.g. gcc or clang)
* CMake
* pkg-config
* MPI (e.g., OpenMPI)
* Suitesparse (install with `sudo apt-get install libsuitesparse-dev` on Debian/Ubuntu systems)

The easiest way to install this module is to create a new folder and to execute the file
[installMichalkowski2021a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/michalkowski2021a/-/raw/master/installMichalkowski2021a.sh)
in this folder.

```bash
mkdir -p Michalkowski2021a && cd Michalkowski2021a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/michalkowski2021a/-/raw/master/installMichalkowski2021a.sh && chmod +x installMichalkowski2021a.sh
sh ./installMichalkowski2021a.sh
```
To build and run the numerical example of Michalkowski & Helmig (2021, submitted), follow these steps:

```bash
cd michalkowski2021a/build-cmake/appl/2p_hydrophilicBC
make appl_pnm_2p_hydrophilicBC
./make appl_pnm_2p_hydrophilicBC params-1Pore.input
./make appl_pnm_2p_hydrophilicBC params-TopPores.input
./make appl_pnm_2p_hydrophilicBC params-GDL.input
```
You may use [ParaView](https://www.paraview.org/) to visualize the results by opening the two `.pvd` files. The `.txt` files contain additional data such as the evaporation rate over time.
